from django.http import JsonResponse
from django.shortcuts import render
from .models import Aboutus

# Create your views here.
content = {}


def about_us(request):
    content['is_login'] = True
    content['data'] = list(Aboutus.objects.all())
    return render(request, 'about_us.html', content)


def cerita(request):
    if request.method == 'POST':
        text = request.POST['text']
        name = request.session['name']
        Aboutus.objects.create(name=name, text=text)
        return JsonResponse({'saved': True})
    return JsonResponse({'saved': False})

def saveCerita(request):
    if request.method == 'POST':
        text = request.POST['text']
        name = request.user.first_name + " " + request.user.last_name
        Aboutus.objects.create(name=name, text=text)
        return JsonResponse({'saved': True, 'name': name, 'text': text})
    return JsonResponse({'saved': False})

