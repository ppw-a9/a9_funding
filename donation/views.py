from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from .forms import FormDonasi
from .models import ModelDonasi
from django.urls import reverse

# Create your views here.
def donasi(request):
	if (request.user.is_authenticated):
		form = FormDonasi(request.POST or None)
		response = {'form' : form, 'D' : ModelDonasi.objects.all()}
		return render(request, 'donasi.html', response)
		
	else:
		return redirect(reverse('login:login'))



def submit(request):
	if(request.method == 'POST'):
		form = FormDonasi(request.POST or None)
		namaProgram = request.POST['namaProgram']
		#nama = request.POST['nama']
		email = request.user.email
		uang = request.POST['uang']
		donasi = ModelDonasi.objects.create(namaProgram=namaProgram, email=email, uang=uang)
		donasi.save()
		return redirect('donation:donasi')
	else:
		return redirect('donation:donasi')

