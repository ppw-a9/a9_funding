from django.db import models


# Create your models here.
class ModelDonasi(models.Model):
	namaProgram = models.CharField(max_length=200)
	email = models.EmailField(max_length=200, null=True)
	uang = models.IntegerField()

