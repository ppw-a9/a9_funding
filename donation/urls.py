from django.contrib import admin
from django.urls import path, include
from .views import donasi, submit

app_name = 'donation'

urlpatterns = [
	path('', donasi, name="donasi"),
	path('fail/', submit, name='submit'),
]