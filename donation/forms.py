from django import forms
from .models import ModelDonasi

class FormDonasi(forms.ModelForm):
	namaProgram = forms.CharField(max_length=100, label='Nama Program', widget=
					forms.TextInput(attrs= {'class' : 'form-control','placeholder' : 'Nama Program' }))
	uang = forms.CharField(label='Jumlah Donasi', widget=
					forms.NumberInput(attrs= {'class' : 'form-control','placeholder' : 'Jumlah Donasi' }))
	class Meta:
		model = ModelDonasi
		fields = ('namaProgram', 'uang')
