'''from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
# Create your tests here.
from .views import donasi, submit
from .models import ModelDonasi


class DonationTest(TestCase):
	def test_donation_url_exist(self):
		response = Client().get('/donasi')
		self.assertEqual(response.status_code, 200)

	def test_donation_using_donation_template(self):
		response = Client().get('/donasi')
		self.assertTemplateUsed(response, 'donasi.html')

	def test_donation_using_donationfail_template(self):
		response = Client().get('/donasifail')
		self.assertRedirects(response, '/donasi')

	def test_donation_func(self):
		found = resolve('/donasi')
		self.assertEqual(found.func, donasi)

	def test_submit_func(self):
		found = resolve('/donasifail')
		self.assertEqual(found.func, submit)

	def test_donation_is_completed(self):
		request = HttpRequest()
		response = donasi(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Nama Program', html_response)
		self.assertIn('Nama', html_response)
		self.assertIn('Email', html_response)
		self.assertIn('Jumlah Donasi', html_response)

	def test_can_d(self):
		ModelDonasi.objects.create(namaProgram="palu", nama="bayu", email="wa@gmail.com", uang="1000000")
		response = self.client.post('/donasifail', data = {
    		'namaProgram' : 'palu',
    		'nama' : 'bayu', 
    		'email' : 'wa@gmail.com', 
    		'uang' : '1000000'
    		})
		counting_all_available_status = ModelDonasi.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)

		response = self.client.post('/donasifail', data = {
    		'namaProgram' : 'we so',
    		'nama' : 'lit', 
    		'email' : 'right@now', 
    		'uang' : '6942069'
    		})
		counting_all_available_status = ModelDonasi.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)
'''