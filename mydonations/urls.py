from django.conf.urls import url
from django.urls import path
from .views import mydonations, get_account_donation

app_name = "mydonation"

urlpatterns = [
    path('', mydonations, name='mydonations'),
    path('get_account_donation/', get_account_donation, name='get_account_donation'),
    # path('get_donation_api/', get_donation_api, name='get_donation_api'),
]