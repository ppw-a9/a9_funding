from django.core import serializers
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from donation.models import ModelDonasi
import requests

# Create your views here.

response = {}
def mydonations(request):
	if (request.user.is_authenticated):
		# If user already login, render the page
		response['title'] = 'A9 Funding - Donasi Saya'		
		response['list_of_donations'] = ModelDonasi.objects.filter(email = request.user.email)
		return render(request, 'mydonations.html', response)
	else:
		return HttpResponseRedirect(reverse('login:login'))

def get_account_donation(request):
	data = ModelDonasi.objects.filter(email = request.user.email)
	data_json = serializers.serialize('json', data)
	# response = requests.get(reverse('mydonations:get_donation_api'))
	# donation_data = response.json()
	donation_queries = {'donations': list()}

	for donation in data_json:
		new_donation = {'nama_program':'', 'uang':''}
		observed_donation = donation['fields']
		# if observed_donation["email"] == request.use
		if 'namaProgram' in observed_donation:
			new_donation['nama_program'] = observed_donation['namaProgram']
		
		if 'uang' in observed_donation:
			new_donation['uang'] = observed_donation['uang']
		
		donation_queries.append(new_donation)
	return JsonResponse(donation_queries)

""" def get_donation_api():
	data = ModelDonasi.objects.all()
	data_json = serializers.serialize('json', data)
	return data_json """
	# return JsonResponse(data, safe=False)