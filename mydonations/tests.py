from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest
from donation.models import ModelDonasi
from .views import mydonations

# Create your tests here.

class MyDonationsUnitTest(TestCase):
    
    def test_mydonations_response(self):
        response = Client().get(reverse('mydonation:mydonations'))
        self.assertEqual(response.status_code,302) # When not logged in
        self.assertNotEqual(response.status_code,404)

    def test_mydonations_template(self):
        response = Client().get(reverse('mydonation:mydonations'))
        # self.assertTemplateUsed(response, 'mydonations.html')

    def test_mydonations_calls_programs_function(self):
        found = resolve(reverse('mydonation:mydonations'))
        self.assertEqual(found.func, mydonations)
