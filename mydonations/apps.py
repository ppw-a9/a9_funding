from django.apps import AppConfig


class MydonationsConfig(AppConfig):
    name = 'mydonations'
