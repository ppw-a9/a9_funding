from django.urls import path
from . import views
# Create your views here.

app_name = 'login'
urlpatterns = [
	path('loged/', views.loged, name = 'loged'),
	path('', views.login, name = 'login'),
	path('logout/', views.logout, name = 'logout'),
]
