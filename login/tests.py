from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from django.contrib.auth.models import User

from .views import login, loged, logout
# Create your tests here.

class LoginUnitTest(TestCase):
	def test_login_url_is_exist(self):
		response = Client().get('/login/')
		self.assertEqual(response.status_code, 200)

	def test_loged_url_is_redirrecting(self):
		response = Client().get('/login/loged/')
		self.assertEqual(response.status_code, 302)

	def test_logout_url_is_redirrecting(self):
		response = Client().get('/login/logout/')
		self.assertEqual(response.status_code, 302)

	def test_login_url_not_exist(self):
		response = Client().get('/login/ppw/')
		self.assertEqual(response.status_code, 404)

	def test_login_using_login_func(self):
		found = resolve('/login/')
		self.assertEqual(found.func, login)   

	def test_login_using_login_template(self):
		response = Client().get('/login/')
		self.assertTemplateUsed(response, 'login.html')

	def test_loged_using_loged_func(self):
		found = resolve('/login/loged/')
		self.assertEqual(found.func, loged)   

	def test_loged_redirect_login_template(self):
		response = Client().get('/login/loged/', follow = "True")
		self.assertTemplateUsed(response, 'login.html')

	def test_logout_using_logout_func(self):
		found = resolve('/login/logout/')
		self.assertEqual(found.func, logout)   

	def test_logout_redirect_login_template(self):
		response = Client().get('/login/logout/', follow = "True")
		self.assertTemplateUsed(response, 'login.html')
