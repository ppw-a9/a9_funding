from django.shortcuts import render
from .models import Program

# Create your views here.

response = {}
def programs(request):
	response['title'] = 'A9 Funding - Program'
	response['all_programs'] = Program.objects.all().values()
	return render(request, 'programs.html', response)