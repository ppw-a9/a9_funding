from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest
from .models import Program
from .views import programs

# Create your tests here.

class ProgramsUnitTest(TestCase):  
    def test_programs_app_response(self):
        response = Client().get(reverse('programs_app:programs'))
        self.assertEqual(response.status_code,200)
        self.assertNotEqual(response.status_code,404)

    def test_programs_app_template(self):
        response = Client().get(reverse('programs_app:programs'))
        self.assertTemplateUsed(response, 'programs.html')

    def test_programs_app_calls_programs_function(self):
        found = resolve(reverse('programs_app:programs'))
        self.assertEqual(found.func, programs)
    
    def test_programs_app_page_is_completed(self):
        request = HttpRequest()
        response = programs(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Donasi', html_response)
        self.assertIn('Program', html_response)
        self.assertIn('Login', html_response)
        self.assertNotIn('Donasi Saya', html_response)
    
    def test_create_object_model(self):
        img_url = 'http://indonesia.travel/content/dam/indtravelrevamp/en/destinations/sulawesi/central-sulawesi/palu/4147d1357339a10916a3bd6a34cb2509835c86ee-425da.jpg/_jcr_content/renditions/cq5dam.web.1280.1280.jpeg'
        title = 'Bantu Korban Gempa di Palu'
        description = 'Bantu perbaikan infrastruktur, pemulihan lingkungan, dan pelayanan medis bagi para korban melalui donasi yang anda berikan.'
        Program.objects.create(image=img_url, title=title, description=description)
        program_count = Program.objects.all().count()
        self.assertEqual(program_count, 1)
