from django.apps import AppConfig


class ProgramsAppConfig(AppConfig):
    name = 'programs_app'
