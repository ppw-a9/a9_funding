from django.db import models

# Create your models here.

class Program(models.Model):
    image = models.ImageField() # kasih default = 'something.jpg'
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=300)