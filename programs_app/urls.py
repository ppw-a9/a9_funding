from django.conf.urls import url
from django.urls import path
from .views import programs


app_name = "programs_app"


urlpatterns = [
    path('', programs, name='programs'),
]