from django.shortcuts import render
from django.http import HttpResponseRedirect

def news(request):
    response = {}
    html = 'index.html'
    return render(request,html,response)
