from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import news

class NewsUnitTest(TestCase):
	def test_news_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)
		
	def test_news_url_not_found(self):
		response = Client().get('/bukan_news/')
		self.assertEqual(response.status_code, 404)
		
	def test_news_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'index.html')
		
	def test_news_using_news_func(self):
		found = resolve('/')
		self.assertEqual(found.func, news)
