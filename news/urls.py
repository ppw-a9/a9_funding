from django.conf.urls import url
from django.urls import path
from .views import news

app_name = "news"

urlpatterns = [
    path('', news, name='news'),
]