# A9_Funding


Repository for Web Programming and Design first assignment.


## A9 members:


| Name                    | NPM        |
|-------------------------|------------|
| Gibran Gifari Soesman   | 1606876462 |
| Alfian Fuadi Rafli      | 1706028650 |
| Kevin Christian Chandra | 1706039976 |
| Bayukanta Iqbal Gunawan | 1706075003 |


## Pipelines status:


[![pipeline](https://gitlab.com/ppw-a9/a9_funding/badges/master/build.svg)](https://gitlab.com/ppw-a9/a9_funding/commits/master)


## Code coverage status:


[![coverage](https://gitlab.com/ppw-a9/a9_funding/badges/master/coverage.svg)](https://gitlab.com/ppw-a9/a9_funding/commits/master)


## Herokuapp link:


https://a9funding.herokuapp.com/